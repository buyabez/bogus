FROM golang:1.13 as builder

WORKDIR /go/src/buyabez/bogus
COPY . /go/src/buyabez/bogus

RUN CGO_ENABLED=0 GOOS=linux go build

FROM alpine:latest
RUN apk --no-cache add ca-certificates
COPY --from=builder /go/src/buyabez/bogus/bogus /usr/bin/bogus 
RUN chmod +x /usr/bin/bogus
ENTRYPOINT ["./usr/bin/bogus"]
