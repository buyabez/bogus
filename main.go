package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"
)

type User struct {
	Id    int    `json:"id"`
	Name  string `json:"name"`
	Email string `json:"email"`
	Phone string `json:"phone"`
}

func bogus(w http.ResponseWriter, r *http.Request) {
	keys, ok := r.URL.Query()["output"]

	if !ok || len(keys[0]) < 1 {
		log.Println("URL param 'output' is missing")
		return
	}

	output := keys[0]
	log.Printf("Outpuut is: '%s'\n", output)
	log.Printf("..stored value in my bogus db\n")
}

func login(w http.ResponseWriter, r *http.Request) {
	user, _ := r.URL.Query()["username"]
	passwd, _ := r.URL.Query()["password"]

	log.Println("User:", user[0])
	log.Println("Password:", passwd[0])
	redirectTo := strings.Split(r.Referer(), "?")[0]
	log.Println("Redict to URL:", redirectTo)

	http.Redirect(w, r, redirectTo, 301)
}

// token returns fakes token data in JSON format
func token(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	user := User{
		Id:    1,
		Name:  "John Doe",
		Email: "johndoe@gmail.com",
		Phone: "000099999",
	}

	json.NewEncoder(w).Encode(user)
}

// token returns fakes token data in JSON format
func token_allow_origin(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	user := User{
		Id:    1,
		Name:  "John Doe",
		Email: "johndoe@gmail.com",
		Phone: "000099999",
	}

	json.NewEncoder(w).Encode(user)
}

func main() {
	log.Println("Running bogus service at 0.0.0.0:8000")
	http.HandleFunc("/bogus", bogus)
	http.HandleFunc("/login", login)
	http.HandleFunc("/token", token)
	http.HandleFunc("/token_allow_origin", token_allow_origin)

	log.Fatal(http.ListenAndServe(":8000", nil))
}
